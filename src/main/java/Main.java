import staff.Administrator;
import staff.Doctor;
import staff.IWorker;
import staff.Technician;
import utils.Injury;
import utils.LastNameGenerator;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeoutException;

/**
 * Created by adakis on 2017-04-05.
 */
public class Main {
    //todo moze kolejny channel na info z admina? chociaz i tak to bez sensu... :P
    public static void main(String[] args) throws IOException, TimeoutException {
        List<IWorker> doctors = new ArrayList<>();
        List<IWorker> technicians = new ArrayList<>();

        int numDoc, numTech;
        Scanner scanner = new Scanner(System.in);

        System.out.println("How many doctors?");
        numDoc = scanner.nextInt();
        System.out.println("How many technicians?");
        numTech = scanner.nextInt();

        IWorker admin = new Administrator();
        initTechs(technicians, numTech);
        initDocs(doctors, numDoc);

        doctors.get(0).publicMessage(Injury.ELBOW, "asadasd");
        doctors.get(1).publicMessage(Injury.ANKLE, "qweqweqwe");

        while(true){
            int i = scanner.nextInt();
            if(i == 1)
                doctors.get(0).publicMessage(Injury.ELBOW, "asadasd");

            else if(i == 2)
                doctors.get(1).publicMessage(Injury.KNEE, "nameee");

            else if(i == 3){
                System.out.println("Choose doctor (there are " +numDoc + " doctors");
                try {
                    int num = scanner.nextInt();
                    if(num <= numDoc && num > 0)
                        doctors.get(num-1).sendInfo();
                    else
                        System.out.println("There is no such doctor!!");
                }catch (InputMismatchException e){
                    System.out.println("Invalid choice, again");
                }
            }

            else if(i == 4)
                admin.sendInfo();

            else if(i == 5){
                System.out.println("WHAT THE HELL IS GOING ON?!?!?!!??!!!111oneoneone");
                Random random = new Random();
                while(true){
                    int doc = random.nextInt(numDoc);
                    int inj = random.nextInt(3);
                    doctors.get(doc).publicMessage(Injury.values()[inj], LastNameGenerator.randomString(10));
                }
            }
            else
                break;
        }
    }

    private static void initTechs(List<IWorker> technicians, int numTech) throws IOException, TimeoutException {
        for(int i = 0; i < numTech; i++) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please insert types of analysis for " + (i+1) + ". technician (KNEE, ANKLE, ELBOW)");

            String injures = scanner.nextLine();
            String[] array = injures.split(" ");

            if(array.length == 2) {
                try {
                    technicians.add(new Technician(i, array));
                } catch (IllegalArgumentException e) {
                    System.out.println("INVALID OPTION!!!");
                    i--;
                }
            }
            else
                i--;
        }
    }

    private static void initDocs(List<IWorker> doctors, int numDoc) throws IOException, TimeoutException {
        for(int i = 0; i < numDoc; i++)
            doctors.add(new Doctor(i));
    }
}
