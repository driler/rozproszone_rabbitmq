package staff;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import utils.Injury;

import java.io.IOException;

/**
 * Created by adakis on 2017-04-07.
 */
public abstract class AbstractWorker implements IWorker{
    @Override
    public void publicMessage(Channel channel, String exchangeName,
                              String key, String msg, String replyTo) throws IOException {
        channel.basicPublish(exchangeName, key,
                new AMQP.BasicProperties.Builder()
                        .replyTo(replyTo)
                        .build(),
                msg.getBytes());
    }
    @Override
    public void publicMessage(Channel channel, String exchangeName,
                              Injury injury, String msg, String replyTo) throws IOException {
        publicMessage(channel, exchangeName, injury.name(), msg, replyTo);
        publicMessage(channel, exchangeName, "logs", msg, replyTo);
    }


    @Override
    public void publicMessage(Channel channel, String exchangeName,
                              Injury injury, String name) throws IOException {
        this.publicMessage(channel, exchangeName, injury, name, "");
    }

    @Override
    public void publicMessage(Channel channel, String exchangeName,
                              String msg, String replyTo) throws IOException {
        this.publicMessage(channel, exchangeName,"", msg, replyTo);
    }

    @Override
    public void publicMessage(Channel channel, String exchangeName,
                              String msg) throws IOException {
        this.publicMessage(channel, exchangeName, msg, "");
    }

    @Override
    public void publicMessage(Injury injury, String name) throws IOException {}

    @Override
    public void sendInfo() throws IOException {}
}
