package staff;

import com.rabbitmq.client.*;
import utils.Injury;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * Created by adakis on 2017-04-05.
 */
public class Doctor extends AbstractWorker {
    private Consumer consumer;
    private Channel channel;
    private List<Injury> injuries;
    private String name;

    public Doctor(int i) throws IOException, TimeoutException {
        this.injuries = Arrays.asList(Injury.values());
        this.name = "doctor_" + i;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();

        this.channel = connection.createChannel();

        this.consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");

                if(properties.getReplyTo().equals("ADMIN"))
                    System.out.println(String.format("  [%s] Message from admin: [%s]",name, message));

                else {
                    System.out.println(String.format("%s got results: %s",
                            envelope.getRoutingKey(), message));
                }
            }
        };


        this.channel.queueDeclare("DOCTOR."+this.hashCode(), false, false, false, null);
        this.channel.queueBind("DOCTOR."+this.hashCode(), "TECHNICIANS", "DOCTOR."+this.hashCode());
        this.channel.basicConsume("DOCTOR."+this.hashCode(), true, this.consumer);

        this.channel.queueDeclare("doctor"+hashCode(), false, false, true, null);
        this.channel.queueBind("doctor"+hashCode(), "ADMIN", "");
        this.channel.basicConsume("doctor"+hashCode(), consumer);
    }

    @Override
    public void sendInfo() throws IOException {
        while (true) {
            System.out.println("Please insert patient's last name:");
            Scanner scanner = new Scanner(System.in);
            String name = scanner.nextLine();

            int i;
            System.out.println("Choose number: 1) knee   2) ankle    3) elbow  ");
            try {
                i = scanner.nextInt();
            } catch (Exception e) {
                i = 0;
            }

            if (i == 1 || i == 2 || i == 3) {
                publicMessage(channel, "DOCTORS", injuries.get(i-1), name, "DOCTOR."+this.hashCode());
                return;
            }
        }
    }

    @Override
    public void publicMessage(Injury injury, String name) throws IOException {
        publicMessage(channel, "DOCTORS", injury, name, "DOCTOR."+this.hashCode());
    }
}
