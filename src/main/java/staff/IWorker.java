package staff;

import com.rabbitmq.client.Channel;
import utils.Injury;

import java.io.IOException;

/**
 * Created by adakis on 2017-04-06.
 */
public interface IWorker {

    void publicMessage(Channel channel, String exchangeName,
                       String key, String msg, String replyTo) throws IOException;

    void publicMessage(Channel channel, String exchangeName,
                       Injury injury, String msg, String replyTo) throws IOException;

    void publicMessage(Channel channel, String exchangeName,
                       Injury injury, String name) throws IOException;

    void publicMessage(Channel channel, String exchangeName,
                       String msg, String replyTo) throws IOException;

    void publicMessage(Channel channel, String exchangeName,
                       String msg) throws IOException;

    void sendInfo() throws IOException;

    void publicMessage(Injury injury, String name) throws IOException;
}
