package staff;

import com.rabbitmq.client.*;
import utils.Injury;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by adakis on 2017-04-05.
 */
public class Technician extends AbstractWorker {
    private List<Injury> injuryList;
    private Channel channel;
    private String EXCHANGE_NAME = "TECHNICIANS";
    private String name;
    private Consumer consumer;


    public Technician(int i, String[] array) throws IOException, TimeoutException {
        this.injuryList = new ArrayList<>();
        this.name = "Technician_" + i;
        for (String anArray : array) {
            this.injuryList.add(Injury.valueOf(anArray));
        }

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();

        this.channel = connection.createChannel();
        this.channel.basicQos(1);
        this.consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                if(properties.getReplyTo().equals("ADMIN"))
                    System.out.println(String.format("  [%s] Message from admin: [%s]",name, message));

                else{
                    System.out.println(String.format("%s injury: %s from: %s patient: %s",
                            name, envelope.getRoutingKey(), properties.getReplyTo(), message));

                    publicMessage(channel, EXCHANGE_NAME, properties.getReplyTo(),
                            message + " -> OK", name);
                    publicMessage(channel, EXCHANGE_NAME, "logs2",
                            message + " -> OK", name);
                }
            }
        };

        this.channel.queueDeclare("technician"+hashCode(), false, false, true, null);
        this.channel.queueBind("technician"+hashCode(), "ADMIN", "");
        this.channel.basicConsume("technician"+hashCode(), consumer);
        injuryList.forEach(it ->{
            try {
                channel.basicConsume(it.name(), true, consumer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


    }

}
