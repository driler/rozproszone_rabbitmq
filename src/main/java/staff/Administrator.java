package staff;

import com.rabbitmq.client.*;
import utils.Injury;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * Created by adakis on 2017-04-05.
 */
public class Administrator extends AbstractWorker{
    private final String EXCHANGE_ADMIN = "ADMIN";
    private List<Injury> injuries;
    private Consumer consumer;
    private Channel channel;
    private final String EXCHANGE_NAME_TECHNICIANS = "TECHNICIANS";
    private final String EXCHANGE_NAME_DOCTORS = "DOCTORS";

    public Administrator() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();

        this.channel = connection.createChannel();
        this.channel.exchangeDeclare(EXCHANGE_NAME_DOCTORS, BuiltinExchangeType.DIRECT);
        this.channel.exchangeDeclare(EXCHANGE_NAME_TECHNICIANS, BuiltinExchangeType.TOPIC);
        this.channel.exchangeDeclare(EXCHANGE_ADMIN, BuiltinExchangeType.FANOUT);

        this.consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(String.format("\tLOGS <%s> from: %s msg: %s",
                        envelope.getExchange(), properties.getReplyTo(), message));
            }
        };

        initQueues();

    }

    private void initQueues() throws IOException {
        Arrays.asList(Injury.values()).forEach(it ->{
            try {
                this.channel.queueDeclare(it.name(), false,false, true, null);
                this.channel.queueBind(it.name(), EXCHANGE_NAME_DOCTORS, it.name());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        this.channel.queueDeclare("logs", false, false, true, null);
        this.channel.queueDeclare("logs2", false, false, true, null);
        this.channel.queueBind("logs", EXCHANGE_NAME_DOCTORS, "logs");
        this.channel.queueBind("logs2", EXCHANGE_NAME_TECHNICIANS, "logs2");
        this.channel.basicConsume("logs", true, consumer);
        this.channel.basicConsume("logs2", true, consumer);
    }

    @Override
    public void sendInfo() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please insert info message:");
        String msg = scanner.nextLine();
        publicMessage(this.channel, "ADMIN", msg, "ADMIN");
    }
}
