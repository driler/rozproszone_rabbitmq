package utils;

/**
 * Created by adakis on 2017-04-05.
 */
public enum Injury {
    KNEE, ELBOW, ANKLE
}
